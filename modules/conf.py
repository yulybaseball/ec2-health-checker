"""
Provides access to configuration files (only JSON for now).
"""

import sys


def get_conf(file_path, type='json'):
    """Return the configuration file content as Python dict.
    If file doesn't exist or is not a valid file, return error.
    """
    current_module = sys.modules[__name__]
    fn = getattr(current_module, '_get_{0}_data'.format(type.lower()), None)
    if fn is not None:
        return fn(file_path)
    return "Type of conf file is not defined."


def _get_json_data(file_path):
    """Loads a JSON-formatted file and return its content as a dictionary."""
    try:
        import json
        with open(file_path) as json_data_file:
            return json.load(json_data_file)
    except Exception as e:
        return "Error loading conf file: {0}".format(str(e))
