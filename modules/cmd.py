"""
Module to execute commands and get resulting output and error.
"""

from os import path
import subprocess
from time import sleep
import socket

# total seconds to wait before checking an application is running after
# executing its executing its script to bring it up and waiting for output
SECONDS2WAIT_BEFORE_CHECKING = 2

# commands
COMMAND_CAT_APPS_ALL = "cat {apps_dot_all_file}"
COMMANDS_STATUS_PROC = [
    "ps -efo pid,args | grep ' \\-D{proc} ' | grep -v grep",
    "ps -efo pid,args | grep -i ' \\-Dsname={proc} ' | grep -v grep",
    "ps -exfo pid,args | grep ' \\-D{proc} ' | grep -v grep",
    "ps -exfo pid,args | grep -i ' \\-Dsname={proc} ' | grep -v grep"
]
COMMANDS_ALL_RUNNING_APPS = [
    "ps -exfo pid,user | grep -v grep",
    "ps -efo pid,user | grep -v grep"
]
COMMANDS_PID_STATUS = [
    "ps -exfo pid | grep {pid} | grep -v grep",
    "ps -efo pid | grep {pid} | grep -v grep"
]
# command to start process should include the 'cd' command because the
# the script to start process includes Java libraries which resides in the
# same directory
COMMAND_START_PROC = "cd {script_dir}; nohup ./{script} >{proc_nohup} 2>&1 &"
COMMAND_KILL_PID = "kill -9 {pid}"
COMMAND_PROC_WORKING_DIR = "pwdx {pid}"


def _pid_status(pid):
    """Search and return the status of 'pid':
        0 - not active
        1 - active
    """
    for command in COMMANDS_PID_STATUS:
        pids, dummy = execute_command(command.format(pid=pid))
        if pids.strip():
            return 1
    return 0


def kill_process(pid):
    """Kill process which id is 'pid'.
    Return:
        0 if pid couldn't be killed
        1 if pid was not active, or it was active but killed
    """
    execute_command(COMMAND_KILL_PID.format(pid=pid))
    sleep(0.2)
    return int(not _pid_status(pid))


def get_all_running_apps(exclude_pids=None):
    """Search all running applications and return them in a dictionay having
    the usernames as keys and a list of pids of the processes started by
    the user:
        {
            <user_started_process>: [<pids>],
            ...
        }
    The resulting list will exclude the pids listed in 'exclude_pids'.
    """
    pids2exclude = exclude_pids or []
    processes_running = ""
    for command in COMMANDS_ALL_RUNNING_APPS:
        processes_running, dummy = execute_command(command)
        if processes_running:
            break
    # result is a string of all processes running, each one on a separate line
    processes_running_list = processes_running.split('\n')
    final_result = {}
    for process_running_item in processes_running_list:
        process_running_item = process_running_item.split(' ')
        # remove empty strings between pid and username
        process_running = [item for item in process_running_item if item]
        # as a result we should have a list of 2 items: [pid,username]
        if len(process_running) == 2:
            pid = process_running[0]
            user = process_running[1]
            # in some systems, the first line of executing the 'ps' command is
            # a header with the names of the fields requested
            if not (pid == "PID" and user == "USER")\
                    and pid not in pids2exclude:
                if user not in final_result:
                    final_result[user] = []
                final_result[user].append(pid)
    return final_result


def get_app_starting_directory(pid):
    """Search and return the working directory of the process id 'pid'."""
    starting_dir, dummy = execute_command(COMMAND_PROC_WORKING_DIR.format(
        pid=pid))
    # result of executing command should be a string like this:
    # <pid>: <working_dir>
    if starting_dir:
        starting_dir_list = starting_dir.split('\t')
        # remove empty strings
        starting_dir_list = [item for item in starting_dir_list if item]
        # check the directory is present
        if len(starting_dir_list) >= 2:
            return starting_dir_list[1]
    return ""


def get_application_all_content(file_path):
    cat_command = COMMAND_CAT_APPS_ALL.format(apps_dot_all_file=file_path)
    return execute_command(cat_command)


def get_pid(app_name):
    """Find and return the process id for 'app_name'.
    Return None if PID is not found.
    """
    process_running = None
    for command_template in COMMANDS_STATUS_PROC:
        command = command_template.format(proc=app_name)
        process_running, dummy = execute_command(command)
        if process_running:
            res_list = process_running.split(' ')
            for res_list_entry in res_list:
                # PID should be the first non-blank string found in the string
                possible_pid = res_list_entry.strip()
                if possible_pid:
                    return possible_pid
    return None


def app_status(app_name):
    """Find out whether app_name is running or not.
    Return 1 if running. 0 otherwise.
    """
    return 1 if get_pid(app_name) else 0


def start_app(app_name, app_start_script):
    """Start app_name by using its start_script and check whether it started
    or not. Return 1 if application is running; 0 otherwise.
    """
    if app_status(app_name):
        return 1
    base_path = path.dirname(app_start_script)
    only_script = path.basename(app_start_script)
    proc_nohup_file = path.join(base_path, 'nohup.out.{0}'.format(app_name))
    command = COMMAND_START_PROC.format(script_dir=base_path,
                                        script=only_script,
                                        proc_nohup=proc_nohup_file)
    dummy, dummy = execute_command(command)
    # some applications run for a couple of seconds and then shut down
    # themselves because of connection issues
    # so let's wait for SECONDS2WAIT_BEFORE_CHECKING seconds before checking
    # againg if app is running
    sleep(SECONDS2WAIT_BEFORE_CHECKING)
    return app_status(app_name)


def stop_app(app_name):
    """Stop application with 'app_name'.
    Return:
        0 if application was not running
        1 if application was running and it was stopped
        2 if application was running and it couldn't been stopped
    """
    pid = get_pid(app_name)
    if pid:
        # we have noticed some processes stay up even after executing
        # the stop action once, so this is a hack for trying to kill it
        kill_attempts = 0
        while kill_attempts < 10:
            kill_process(pid)
            sleep(0.2)
            if not app_status(app_name):
                return 1
            kill_attempts += 1
        return 2
    return 0


def execute_command(command, get_output=True):
    """Excute command and return the output if specified."""
    if get_output:
        p = subprocess.Popen(command, shell=True, stdin=subprocess.PIPE,
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = p.communicate()
        if type(output) is bytes:
            output = output.decode('utf-8')
        if type(error) is bytes:
            error = error.decode('utf-8')
        return output.strip(), error.strip()
    else:
        subprocess.Popen(command, stdin=subprocess.PIPE, shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def current_server():
    """Get and return the name of current server. Convert name to string and
    remove all break lines and spaces. On error, return empty string.
    """
    try:
        server = socket.gethostname()
        if type(server) is bytes:
            server = server.decode('utf-8')
        server = server.replace('\r', '').replace('\n', '').strip()
    except Exception:
        server = ""
    return server
