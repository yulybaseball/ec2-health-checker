"""
Creates logger with specific formatter and return it.
"""

import logging
from logging.handlers import TimedRotatingFileHandler


def load_logger(log_file_name):
    """Create logger with name logger_name and return it."""
    logger = logging.getLogger("health-checker")
    logger.setLevel(logging.DEBUG)
    logger_fmtter = logging.Formatter(
                    fmt='%(asctime)s - %(levelname)s - %(message)s')
    try:
        if not logger.handlers:
            # rotate logs every day at midnight and keep them for 2 weeks
            handler_info = TimedRotatingFileHandler(log_file_name,
                                                    when='midnight',
                                                    backupCount=14)
            handler_info.setFormatter(logger_fmtter)
            logger.addHandler(handler_info)
    except Exception:
        # we can ignore the exception
        pass
    return logger
