"""
Module to manage all needed AWS S3 services.
"""

import boto3
from datetime import datetime


def _generate_file_name(ec2_name):
    """Generates a file name requested by an EC2 instance with name 'ec2_name'.
    File name should have this form:
        <ec2_name>-<current_datetime>
    """
    datetime_txt = datetime.now().strftime("%d%m%Y-%H%M%S")
    return "{0}-{1}.json".format(ec2_name, datetime_txt)


def upload_file2bucket(ec2_requester, file_content, bucket):
    """Creates and uploads a file to AWS S3.

    Keyword arguments:

    ec2_requester -- name of the EC2 instance requesting to upload the file.
    file_content -- content of the file to be uploaded, in json format.
    bucket -- name of the bucket the file is going to be uploaded to.
    """
    error = ""
    try:
        s3 = boto3.client('s3')
        s3.put_object(
            Body=file_content,
            Bucket=bucket,
            Key=_generate_file_name(ec2_requester)
        )
    except Exception as e:
        error = str(e)
    return error
