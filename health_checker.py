#!/bin/env python

"""
Application to check health of applications running in EC2 instances.

##############################  DISCLAIMER  ##################################
This application was created specifically to be used by Tracfone's Application
Integration team, so this MIGHT NOT be the health check application you need.

By running it, you agree you are totally responsible for what it does and its
output. Please note this application was not created by Tracfone developers,
so be cautious when executing it (this is not an enterprise-level application).

You are totally free to use, modify or fork this application; don't need the
consent of its developer. You can even say you made it!!

Any issue or suggestion? Contact this guy: yulybaseball@tutanota.com
##############################################################################

Usage:

./health_checker.py [-1 <0|1>] [-0 <0|1>] [-a <app-name>] [--info] [--print]

Options passed through parameters takes precedence and overwrites those in the
configuration file.

Parameters:
    -1: either 0 or 1, indicating to this script whether to check or not the
        applications that should be running.
    -0: either 0 or 1, indicating to this script whether to check or not the
        applications that should NOT be running. If either this or the config
        option is set to check non-mandotory applications, this script will
        terminate (kill) whichever is running.
    -a: check the status of <app-name>. By default, an action is taken,
        depending on the status of this application.
    --info: no action is taken, only the resulting info from the checkings is
        returned and/or printed.
    --print: print the final result to the standard output.

This script relies on configuration file ./config/conf.json, and writes
to the log file ./logs/check_processes.log
If log file couldn't be used, log will be sent to 'error.log' file under same
directory as this script.

Configuration file will define the actions to be taken. It is a JSON file with
this structure (all keys are optional, except the path to the applications.ALL
file):

{
    "applications": {
        "applications.ALL": <path2file>,
        "check_mandatory_1": <true|false>,
        "check_mandatory_0": <true|false>,
        "check_times": <number>,
        "interval_between_checkings": <number>,
        "running_patterns": {
            <username>: [<paths>]
        },
        "start_apps": [<*|app_names>],
        "keep_checking": <number>
    },
    "external_actions": {
        "action": <info|stop|terminate|detach>,
        "always_send_info": <true|false>,
        "on_quantity": <number>,
        "on_percentage": <number>,
        "on_running_not_mandatory_app": <true|false>,
        "bucket": <bucket_name>,
        "logs2cloudwatch": <true|false>
    }
}

"applications.ALL": complete path to the applications.ALL file.
"check_mandatory_1": defines whether to check or not the applications that
    should be running (mandatory apps).
"check_mandatory_0": defines whether to check or not the applications that
    should NOT be running. All applications that should not be running will be
    killed.
"check_times": maximum number of times this script will check the applications
    that should be running before taking an action (action will be taken if
    there is at least 1 mandatory application that is not running).
"interval_between_checkings": seconds to wait between checkings.
"running_patterns": dictionary containing the usernames and directories
    under which the applications should be running, being the keys the
    'usernames' and the values a list of directories. If defined, and if
    'check_mandatory_0' is also defined and set to true, all processes running
    under these usernames and directories, which are not listed in the
    applications.ALL as 'mandatory' applications (having a '1'), could be
    terminated (killed).
"start_apps": list containing the name of the applications that this script
    will try to bring up if they are down and they are marked with a '1' in
    the applications.ALL (mandatory apps). Empty list by default (no apps will
    be started up). Set an asterisk (*) for including all mandatory apps.
"keep_checking": minutes to wait before running this application again. If
    defined, this application won't exit, but it will sleep this amount of
    minutes until the next run. Zero (0) means 'do not keep checking'.
"external_actions": defines the configuration options for the external
    application that will handle the actions to be taken with the instance
    running this application. IMPORTANT: Do not include this section in the
    configuration file if you don't want an external application takes any
    action with the instance.
"action": Either 'info', 'stop', 'terminate' or 'detach', which are the
    specific actions the external application should be able to take with
    the instance running this application.
    'info': only sends the result of executing this applications (default).
    'detach': detaches the instance from the auto-scaling group, if any.
    'stop': stops the instance.
    'terminate': terminates the instance.
"always_send_info": if true (false by default), this application will try to
    upload the final result outputs to AWS bucket, regardless of the value of
    'action' or any other specification (it only needs the bucket to be defined
    in order to upload the result). If not action is supposed to be taken by
    the external application, 'info' will be sent as action.
"on_quantity": indicates the minimum amount of applications
    that are not running when they should, before taking the action.
    i.e. let's say this value is set to 3, and there are 20 apps in the
    applications.ALL marked with '1', but there are 4 of them that are NOT
    running; since 3 <= 4, any action specified under the 'action' key will
    be sent to an external application (if other conditions are met). If
    this and 'on_percentage' options are both defined, any of them that
    result in a positive evaluation will trigger the external action. If
    none of these options were defined, action will be sent to an external
    application if at least one mandatory application is down.
"on_percentage": indicates the minimum percentage of applications that are not
    running when they should, before sending the action to an external
    application (if other conditions are met). If this and 'on_quantity'
    options are both defined, any of them that result in a positive evaluation
    will trigger the external action. If none of these options were defined,
    action will be sent to an external application if at least one mandatory
    application is down.
"on_running_not_mandatory_app": Indicates if action should be taken by the
    external application when at least one not mandatory application is running
    and couldn't be killed. This option is evaluated only if all mandatory
    applications are running. False by default.
"bucket": name of the bucket used for uploading a json file with the result
    of executing this application.
"logs2cloudwatch": indicates if the external application will log the result
    in AWS CloudWatch service. False by default.

While executing, this application updates the global variable 'final_result',
which is a dictionary containing the information that the external application
will need in order to take further actions. The content of this global variable
will be sent (if applicable) to a JSON file in the bucket specified in the
configuration file. Structure of 'final_result':

{
    "instance_name": <string>,
    "messages": [{'message': <string>, 'message_type': <string>}],
    "logs2cloudwatch": <true|false>,
    "mandatory_apps_result": {
        "action": <true|false>,
        "messages": {
            <app_name>: <string>
        }
    },
    "not_mandatory_apps_result": {
        "action": <true|false>,
        "messages": {
            <app_name>: <string>
    },
    "action": <info|stop|terminate|detach>
}

"instance_name": name of the instance (hostname) where this application is
    running on.
"messages": messages of general purposes, commonly used to specify general
    errors and warnings.
"logs2cloudwatch": whether the external applications should or not to send the
    messages to 'Cloud Watch' service.
"mandatory_apps_result":
    "action": whether is it necessary to take action with any (at least one) of
    the mandatory applications.
    "messages": specific messages for mandatory applications.
"not_mandatory_apps_result": (same as 'mandatory_apps_result', but for
    not mandatory applications).
"action": the action to be taken by the external application.

Important: it is encouraged that the external application doesn't assume all
keys are always present in the given result inside the JSON file.
"""

import getopt
import json
from multiprocessing import Lock, Process, Queue
import os
import sys
from time import sleep


def _print2file_on_error(error):
    """Print error message to file 'error.log'.
    Function to be used only if 'logger' object is not defined or it fails when
    using it.
    """
    from datetime import datetime as dt
    with open("error.log", 'a+') as error_log:
        error_log.write("{0} - {1}\n".format(dt.now(), error))


def _print_result_json(param_options, result):
    """Print dictionary result as a JSON-formatted text."""
    try:
        if 'print' in param_options:
            print(json.dumps(result))
    except Exception as e:
        _print2file_on_error(str(e))


try:
    EXEC_DIR = os.path.dirname(os.path.realpath(__file__))
    CONF_FILE = os.path.join(EXEC_DIR, 'conf', 'conf.json')
    LOG_FILE = os.path.join(EXEC_DIR, 'logs', 'check-processes.log')
    MODULES_PATH = os.path.join(EXEC_DIR, 'modules')
    # include our customed modules path in current Python path
    sys.path.append(MODULES_PATH)
    # import our customed modules
    from modules import conf, logger as logmod, cmd, s3accessor
except Exception as e:
    error = "There was a critical error when running this script: {0}".\
        format(str(e))
    _print2file_on_error(error)
    exit(1)

# options available to be passed as parameters
ARGUMENT_OPTIONS = "0:1:a:"
NO_ARGUMENT_OPTIONS = ["info", "print"]
PARAMETERS_CONFIF_MAP = {
    "-1": "check_mandatory_1",
    "-0": "check_mandatory_0",
    "-a": "a",
    "--info": "info",
    "--print": "print"
}

# default config values
CHECK_TIMES = 1
INTERVAL_BETWEEN_CHECKINGS = 3

# available actions to be taken by an external application
AVAILABLE_EXTERNAL_ACTIONS = ['info', 'detach', 'stop', 'terminate']

# results from trying to stop/kill applications
SHUTTING_APPS_COMMAND_RESULT = {
    "0": {
        "message": "not running",
        "m_type": "info"
    },
    "1": {
        "message": "was running, but it was killed; not running now",
        "m_type": "warning"
    },
    "2": {
        "message": "it is running, it couldn't be killed",
        "m_type": "warning"
    }
}

# global variables
logger = None
secured_action_lock = None
mandatory_apps_result = None
not_mandatory_apps_result = None
mandatory_apps_quantities = None
final_result = None


def _log(message, m_type='info'):
    """Log message if global object logger exists."""
    try:
        if logger:
            method_by_message_type = {
                'info': logger.info,
                'warning': logger.warning,
                'error': logger.error
            }
            method_by_message_type[m_type](message)
        else:
            raise Exception('No logger exists.')
    except Exception:
        _print2file_on_error(message)


def _init():
    """Create logger. Initialize checking process."""
    global logger
    logger = logmod.load_logger(LOG_FILE)
    global secured_action_lock
    secured_action_lock = Lock()
    global not_mandatory_apps_result
    not_mandatory_apps_result = {
        "action": False,
        "messages": {}
    }
    global mandatory_apps_result
    mandatory_apps_result = {
        "action": False,
        "messages": {}
    }
    global mandatory_apps_quantities
    mandatory_apps_quantities = {
        "total": 0,
        "down": 0
    }
    global final_result
    final_result = {
        "messages": [],
        "logs2cloudwatch": False
    }
    _log("Starting the checkings...")


def _parse_options():
    """Parse and return the options passed as parameters.
    Result dictionary has this form:
        {
            'check_mandatory_1': <1|0>,
            'check_mandatory_0': <1|0>,
            'a': <application_name>,
            'info': '',
            'print': <true|false>
        }
    """
    _log("Parsing options...")
    optlist, dummy = getopt.getopt(sys.argv[1:], ARGUMENT_OPTIONS,
                                   NO_ARGUMENT_OPTIONS)
    options = {}
    for option in optlist:
        options[PARAMETERS_CONFIF_MAP[option[0]]] = option[1]
    _log("Options: {0}".format(options))
    return options


def _load_applications_all(conf_options):
    """Load the applications.ALL file into 2 lists. First one contains the apps
    that are supposed to be running. List of dictionaries of this form:
        [
            {app1name: <script2start>},
            {app2name: <script2start>},
            ...
        ]
    where app#name means the name of the process in the applications.ALL file
    and, script2start is the value found in the applications.ALL for starting
    up the app.
    Second list holds the names of the applications listed in applications.ALL
    that are not supposed to be running (marked with a '0').
    Return both lists.
    """
    _log("Loading applications.ALL...")
    path_app_all = conf_options["applications"]["applications.ALL"]
    stdout, stderror = cmd.get_application_all_content(path_app_all)
    if stderror:
        raise Exception(stderror)
    # assuming applications.ALL file has this content:
    # <app_name>:<process_start_script>:<0|1>
    apps_all_1 = []
    apps_all_0 = []
    for line in stdout.split('\n'):
        _l = line.strip()
        if _l:
            line_data = line.split(':')
            if line_data[-1] == '1':
                apps_all_1.append({line_data[0]: line_data[1]})
            else:
                apps_all_0.append(line_data[0])
    return apps_all_1, apps_all_0


def _get_key_value(somedict):
    """Parse and return 'key,value' of 'somedict'."""
    # next 3 lines exist to keep compatibility between different
    # versions of Python
    _d_items = somedict.items()
    if type(_d_items) is not list:
        _d_items = list(_d_items)
    key, value = _d_items[0]
    return key, value


def _add_final_message(message, m_type):
    """Add a dictionary to the list of final messages into the global variable
    'final_result'.
    """
    final_result["messages"].append({
        "message": message,
        "message_type": m_type
    })


def _thread_worker(app_name, script2start_app, result_queue):
    """Start app_name by executing script2start_app."""
    start_app_result = cmd.start_app(app_name, script2start_app)
    action = False
    if start_app_result:
        message = "'{0}' was brought up.".format(app_name)
        message_type = 'info'
    else:
        message = "'{0}' could NOT be brought up.".format(app_name)
        message_type = 'warning'
        action = True  # action shoud be taken since mandatory app is down
    result_queue.put({
        "app": app_name,
        "message": message,
        "action": action
    })
    with secured_action_lock:
        _log(message, message_type)


def _bring_up_mandatory_apps(mandatory_apps_down, start_apps_option):
    """Bring up the mandatory apps and keep track of the result by updating
    the global variable 'start_apps_result'.
    """
    processes = []
    result_queue = Queue()
    for down_app in mandatory_apps_down:
        # app_name, script = down_app.items()[0]
        app_name, script = _get_key_value(down_app)
        if start_apps_option[0] == '*' or app_name in start_apps_option:
            _log("Trying to bring '{0}' up...".format(app_name))
            process = Process(target=_thread_worker, args=(app_name, script,
                                                           result_queue))
            processes.append(process)
            process.start()
    for process in processes:
        process.join()
    # update the global variable for keeping the results of bringing apps up
    while not result_queue.empty():
        queue_item = result_queue.get()
        if queue_item['action']:
            # means mandatory app couldn't be brought up
            # action is needed
            mandatory_apps_result['action'] = True
            # increase the amount of mandatory apps which are down
            mandatory_apps_quantities['down'] += 1
        app_name = queue_item['app']
        message = queue_item['message']
        mandatory_apps_result['messages'][app_name] = message


def _get_not_running_apps(nr_apps):
    """Return a list with the mandatory applications that are not running."""
    not_running = []
    for nr_app in nr_apps:
        app_name, script = _get_key_value(nr_app)
        app_status = cmd.app_status(app_name)
        status_txt = "running" if app_status else "not_running"
        _log("Checking app '{0}'... {1}".format(app_name, status_txt))
        if not app_status:
            not_running.append(nr_app)
            # update global varibale 'mandatory_apps_result'
            mandatory_apps_result['messages'][app_name] = status_txt
    return not_running


def _find_mandatory_app_start_script(mandatory_apps, app_name):
    """Find the script to start 'app_name' up.
    It is specified in the applications.ALL file, which was dump into the
    parameter 'mandatory_apps' as a list of dictionaries."""
    for mandatory_app in mandatory_apps:
        _app_name, script = _get_key_value(mandatory_app)
        if _app_name == app_name:
            return script
    return ""


def _check_mandatory_apps(mandatory_apps, start_apps, times, interval, opts):
    """Check mandatory apps are running. Bring them up if specified in the
    configuration file.
    """
    # if one app was passed as parameter, only that app will be checked
    if 'a' in opts and opts['a']:
        app_name = opts['a']
        mandatory_apps = [{
            app_name: _find_mandatory_app_start_script(mandatory_apps, app_name)
        }]
    not_running_apps = mandatory_apps
    for _t in range(1, times + 1):
        _log("Checking mandatory apps: {0} out of {1} times".format(_t, times))
        not_running_apps = _get_not_running_apps(not_running_apps)
        if not not_running_apps:
            break
        if _t < times:
            _log("At least one app is not running...", 'warning')
            _log("Sleeping for {0} seconds...".format(interval))
            sleep(interval)
    if not_running_apps:
        _log("There is at least one application that is supposed to be " +
             "running and it is not.", 'warning')
        if 'info' not in opts and start_apps:
            # only bring applications up if at least one mandatory app is
            # down and '--info' was not passed as parameter and 'start_apps'
            # option has at least one app defined in the conf file
            _bring_up_mandatory_apps(not_running_apps, start_apps)
        elif 'info' in opts:
            message = "No action taken since '--info' was specified."
            _add_final_message(message, 'info')
            _log(message)
    else:
        _log("All 'mandatory' applications are running!")


def _terminate_unsolicited_apps(not_mandatory_apps, mandatory_apps,
                                apps_running_patterns, opts):
    """Terminate (kill) all applications that should not be running: those
    that match the user and working path defined in 'apps_running_patterns'
    and are not between those marked with one (1) in the applications.ALL file.
    """
    mandatory_apps_pids = []
    for mandatory_app in mandatory_apps:
        # app_name, dummy = mandatory_app.items()[0]
        app_name, dummy = _get_key_value(mandatory_app)
        pid = cmd.get_pid(app_name)
        if pid is not None:
            mandatory_apps_pids.append(pid)
    # running applications, without the 'mandatory' ones included
    all_running_apps = cmd.get_all_running_apps(mandatory_apps_pids)
    for user in apps_running_patterns:
        user_apps_pids = all_running_apps[user] if user in all_running_apps\
            else []
        for pid in user_apps_pids:
            user_app_directory = cmd.get_app_starting_directory(pid)
            # if application, which is a 'non-mandatory' (unsolicited) one, is
            # running from one of the directories supposed to be used only by
            # 'mandatory' applications, then it should be terminated
            for directory in apps_running_patterns[user]:
                if user_app_directory.startswith(directory):
                    _log(("Application with ID {0} is running under user {1}" +
                          "from the directory {2}; it is NOT listed as one" +
                          "of the applications that should be running.").
                         format(pid, user, user_app_directory), 'warning')
                    if 'info' in opts:
                        message = ("No action will be taken since '--info' " +
                                   "was specified.")
                        _log(message, 'warning')
                    else:
                        _log("Trying to kill pid {0}...".format(pid))
                        result = cmd.kill_process(pid)
                        message = SHUTTING_APPS_COMMAND_RESULT[str(result)]['message']
                        if result == 0:  # app still running
                            not_mandatory_apps_result['action'] = True
                            _log(message, 'warning')
                    # update global variable always for any not mandatory
                    # app which is running
                    not_mandatory_apps_result['messages']["{0} {1} {2}".format(
                        pid, user, user_app_directory
                    )] = message


def _check_not_mandatory_apps(not_mandatory_apps, mandatory_apps,
                              apps_running_patterns, opts):
    """Check applications marked with a zero (0) in applications.ALL file are
    not running. If 'running_patterns' is defined in the configuration file,
    check no application (other than those marked with one (1) in the
    applications.ALL) is actually running.
    """
    # check applications marked with a zero (0) in the applications.ALL file
    # kill whichever is running
    for app_name in not_mandatory_apps:
        result = cmd.stop_app(app_name)
        message = SHUTTING_APPS_COMMAND_RESULT[str(result)]['message']
        m_type = SHUTTING_APPS_COMMAND_RESULT[str(result)]['m_type']
        _log("Checking app '{0}'... {1}".format(app_name, message), m_type)
        # update global variable only if application was running (1 or 2)
        if result in [1, 2]:
            not_mandatory_apps_result[app_name] = result
    # check other apps running under specific directory/username that should
    # not be running
    if apps_running_patterns:
        _log("Checking for unsolicited applications running under " +
             "configured running patterns: {0}".format(apps_running_patterns))
        _terminate_unsolicited_apps(not_mandatory_apps, mandatory_apps,
                                    apps_running_patterns, opts)


def _check_applications(options, mandatory_apps, not_mandatory_apps,
                        config_options):
    """Check applications that should be running are actually running, and
    those which are not supposed to be running are actually down.

    Keyword arguments:

    options -- dictionary with the options passed as arguments to this script.
    mandatory_apps -- dictionary with applications that should be running,
        as defined in the applications.ALL file.
    not_mandatory_apps -- list of applications defined with a zero (0) in
        the applications.ALL file. These ones should not be running.
    config_options -- dictionary with the configurations defined in the
        conf.json file."""
    # check options and configuration values to decide whether mandatory apps
    # are supposed to be checked
    if ('check_mandatory_1' in options and options['check_mandatory_1'] == '1')\
            or ('check_mandatory_1' not in options and 'check_mandatory_1' in
                config_options['applications'] and
                config_options['applications']['check_mandatory_1']):
        _log("'Mandatory' apps are set to be checked.")
        start_apps = config_options['applications']['start_apps'] if 'start_apps'\
            in config_options['applications'] else []
        times = config_options['applications']['check_times'] if 'check_times'\
            in config_options['applications'] else CHECK_TIMES
        intervals = config_options['applications']['interval_between_checkings']\
            if 'interval_between_checkings' in config_options['applications']\
            else INTERVAL_BETWEEN_CHECKINGS
        _check_mandatory_apps(mandatory_apps, start_apps, times, intervals,
                              options)
    else:
        message = "'Mandatory' applications are NOT set to be checked."
        m_type = 'warning'
        _add_final_message(message, m_type)
        _log(message, m_type)
    # check options and configuration values to decide whether not mandatory
    # apps are supposed to be checked
    if ('check_mandatory_0' in options and options['check_mandatory_0'] == '1')\
            or ('check_mandatory_0' not in options and 'check_mandatory_0' in
                config_options['applications'] and
                config_options['applications']['check_mandatory_0']):
        _log("Not 'mandatory' applications are set to be checked.")
        apps_running_patterns = config_options['applications']['running_patterns']\
            if 'running_patterns' in config_options['applications'] else None
        _check_not_mandatory_apps(not_mandatory_apps, mandatory_apps,
                                  apps_running_patterns, options)
    else:
        message = "'Not Mandatory' applications are NOT set to be checked."
        m_type = 'warning'
        _add_final_message(message, m_type)
        _log(message, m_type)


def _determine_action_ext_app(ext_opts):
    """Determine and return the action that the external application is
    supposed to execute. Return empty string if the action is not defined as
    the actions available to be executed by the external application.
    """
    action = ''
    on_running_nmapp = ext_opts['on_running_not_mandatory_app']\
        if 'on_running_not_mandatory_app' in ext_opts else False
    if mandatory_apps_result['action'] or\
            (not_mandatory_apps_result['action'] and on_running_nmapp):
        action = ext_opts['action'] if 'action' in ext_opts else 'info'
    if action and action not in AVAILABLE_EXTERNAL_ACTIONS:
        _log(("Action '{0}' is not allowed to be executed by an " +
              "external application.").format(action), 'warning')
        return ''
    return action


def _action_on_quantity(ext_opts):
    """Find out if the amount of mandatory applications that are down is equal
    or greater than the value configured in the configuration file.

    Keyword arguments:
    ext_opts -- dictionary containing the part of the configuration file
        related only to the configuration to be used for the external
        application. Refer to the main documentation on the top of this file
        for a complete explanation of the keys and the format of this part of
        the configuration file.

    Return True if action can be taken; False otherwise. Will return True if
    no option was specified in the configuration file for checking the amount
    of mandatory apps that are down.
    """
    ma_tot = mandatory_apps_quantities['total']
    ma_down = mandatory_apps_quantities['down']
    if ('on_quantity' not in ext_opts and 'on_percentage' not in ext_opts) or\
            ('on_quantity' in ext_opts and ma_down >= ext_opts['on_quantity']) or\
            ('on_percentage' in ext_opts and ma_down >= (ma_down * 100 / ma_tot)):
        return True
    _log("Quantities to take action were not met.")
    return False


def _send_info2bucket(conf_options):
    """Create and upload a file with the 'final_result' content to AWS S3.

    Keyword arguments:

    conf_options -- all options in the configuration file.

    'final_result' is a global dictionary with the content of the resulting
    checkings and actions carried out by this application which will be used
    as the configuration options for the external application.
    Refer to the general documentation of this application on the top of this
    file for a complete explanation of key/values of 'final_result'.
    """
    try:
        if 'external_actions' in conf_options and\
                'bucket' in conf_options['external_actions'] and\
                conf_options['external_actions']['bucket']:
            ext_conf = conf_options['external_actions']
            if 'logs2cloudwatch' in ext_conf:
                final_result['logs2cloudwatch'] = ext_conf['logs2cloudwatch']
            action = _determine_action_ext_app(ext_conf)
            always_send_info = ext_conf['always_send_info']\
                if 'always_send_info' in ext_conf else False
            # send info to bucket if this happens:
            # - action has a value and the 'quantities' are True
            # - or 'always_send_info' was defined as True
            if (action and _action_on_quantity(ext_conf)) or always_send_info:
                if not action:
                    action = 'info'
                final_result['action'] = action
                bucket = ext_conf['bucket']
                _log("Sending info to bucket '{0}'".format(bucket))
                current_server = cmd.current_server() or "no-server-name"
                final_result['instance_name'] = current_server
                info2bucket_result = s3accessor.upload_file2bucket(
                    current_server, json.dumps(final_result), bucket)
                if info2bucket_result:
                    raise Exception(info2bucket_result)
                _log("Info was successsfuly uploaded to bucket '{0}'.".
                     format(bucket))
            else:
                _log("Nothing to be sent to the bucket.")
        else:
            _log("No external action will be taken since either " +
                 "'external_actions' or 'bucket' key is missing in the " +
                 "configuration file.", 'warning')
    except Exception as e:
        _log("Couldn't upload result to bucket because of this exception: {0}".
             format(str(e)), 'error')


def _finish_executing(error, param_options, config_options):
    """Do what is needed before finishing execution, even if there was an
    exception.
    """
    # log exeception (if any)
    if error:
        _add_final_message(error, 'error')
        _log(error, 'error')
    # fill the final result global variable with the general output
    final_result['mandatory_apps_result'] = mandatory_apps_result
    final_result['not_mandatory_apps_result'] = not_mandatory_apps_result
    # create file into AWS bucket in order to notify external applications
    _send_info2bucket(config_options)
    _log("Finished executing.")


def _main():
    while (True):
        options = {}
        conf_dict = {}
        error = ""
        apps_all_1 = []
        try:
            _init()
            # parse options passed as parameter
            options = _parse_options()
            # load configuration file in ./conf/conf.json
            _log("Loading configuration file...")
            conf_dict = conf.get_conf(CONF_FILE)
            if type(conf_dict) is str:
                raise Exception(conf_dict)
            _log("Configuration file loaded successfuly.")
            # load applications.ALL
            apps_all_1, apps_all_0 = _load_applications_all(conf_dict)
            _log("applications.ALL loaded successfuly.")
            # update global variable with the amount of mandatory apps
            mandatory_apps_quantities['total'] = len(apps_all_1)
            # check applications
            _check_applications(options, apps_all_1, apps_all_0, conf_dict)
        except KeyError as ke:
            error = "Key '{0}' does not exist in the configuration file.".\
                format(str(ke))
        except Exception as e:
            error = str(e)
        finally:
            _finish_executing(error, options, conf_dict)
            _print_result_json(options, final_result)
        try:
            if 'keep_checking' in conf_dict['applications']:
                checking_interval = conf_dict['applications']['keep_checking']
                if type(checking_interval) is int and checking_interval >= 1 \
                        and checking_interval <= 10080:
                    # between 1 minute and one week interval
                    sleep(checking_interval * 60)
                    continue
            raise Exception("Do not keep checking!")
        except Exception:
            break


if __name__ == "__main__":
    _main()
